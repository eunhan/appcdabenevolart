/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { GererCompteComponent } from './gerer-compte.component';

describe('GererCompteComponent', () => {
  let component: GererCompteComponent;
  let fixture: ComponentFixture<GererCompteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GererCompteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GererCompteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
