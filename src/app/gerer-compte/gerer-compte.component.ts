import { Router } from "@angular/router";
import { Utilisateur } from "./../shared/models/utilisateur.model";
import { Component, OnInit } from "@angular/core";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { AuthentificationService } from "../shared/services/authentification.service";
import {
  emailValidtor,
  passValidator,
  telephoneValidator
} from "../shared/validator/validator";
import { Authentification } from "../shared/models/authentification.model";

@Component({
  selector: "app-gerer-compte",
  templateUrl: "./gerer-compte.component.html",
  styleUrls: ["./gerer-compte.component.css"]
})
export class GererCompteComponent implements OnInit {
  public authentification: Authentification;
  public formGroup: FormGroup;
  public utilisateur: Utilisateur;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthentificationService,
    private router: Router
  ) {
    this.authentification = new Authentification(null, "", "", null);
    this.utilisateur = new Utilisateur(
      null,
      "",
      "",
      0,
      false,
      null,
      null,
      null
    );
  }

  ngOnInit() {
    //this.utilisateur = JSON.parse(sessionStorage.getItem("session"));

    this.authService.utilisateur.subscribe((utilisateur: Utilisateur) => {
      this.utilisateur = utilisateur;
      console.log(this.utilisateur);

      this.authService
        .getAuthentification(utilisateur.id)
        .subscribe((authentification: Authentification) => {
          this.authentification = authentification;
          console.log(this.authentification);
        });

      /*this.authService.authentification.subscribe(
        (authentification: Authentification) => {
          this.authentification = authentification;
          console.log(this.authentification);
        }
      );*/
    });

    this.formGroup = this.formBuilder.group({
      pseudo: [
        this.utilisateur.pseudo,
        [Validators.required, Validators.minLength(4), Validators.maxLength(10)]
      ],
      // email: [this.authentification.email, emailValidtor],
      motPasse: this.authentification.motPasse,
      confirmMdp: ["", passValidator],
      telephone: [this.utilisateur.telephone, telephoneValidator]
    });
    this.formGroup.controls["motPasse"].valueChanges.subscribe(x =>
      this.formGroup.controls["confirmMdp"].updateValueAndValidity()
    );
  }

  onSubmit() {
    this.authentification.utilisateur = this.utilisateur;
    this.authService.modifier(this.authentification);
    sessionStorage.setItem("session", JSON.stringify(this.utilisateur));
    this.router.navigate(["/rechercher"]);
  }
}
