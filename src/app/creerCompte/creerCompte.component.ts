import { Utilisateur } from "./../shared/models/utilisateur.model";
import { Authentification } from "./../shared/models/authentification.model";
import { FormGroup, Validators, FormBuilder } from "@angular/forms";
import { AuthentificationService } from "./../shared/services/authentification.service";
//import { FormBuilder } from "@angular/forms";
import { Component, OnInit } from "@angular/core";
import {
  passValidator,
  emailValidtor,
  telephoneValidator
} from "../shared/validator/validator";
import { Router } from "@angular/router";

@Component({
  selector: "app-creerCompte",
  templateUrl: "./creerCompte.component.html",
  styleUrls: ["./creerCompte.component.css"]
})
export class CreerCompteComponent implements OnInit {
  public formGroup: FormGroup;
  public authentification: Authentification;
  public utilisateur: Utilisateur;
  public verify: boolean = true;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthentificationService,
    private router: Router
  ) {
    this.authentification = new Authentification(null, "", "", null);
    this.utilisateur = new Utilisateur(null, "", "", 0, true, null, null, null);
  }

  ngOnInit() {
    this.formGroup = this.formBuilder.group({
      pseudo: [
        "",
        [Validators.required, Validators.minLength(4), Validators.maxLength(10)]
      ],
      email: ["", emailValidtor],
      motPasse: "",
      confirmMdp: ["", passValidator],
      telephone: ["", telephoneValidator]
    });
    this.formGroup.controls["motPasse"].valueChanges.subscribe(x =>
      this.formGroup.controls["confirmMdp"].updateValueAndValidity()
    );
  }

  onSubmit() {
    this.authentification.utilisateur = this.utilisateur;
    this.authService
      .inscription(this.authentification)
      .subscribe((verify: boolean) => {
        this.verify = verify;
        this.verifyExist();
      });
  }

  verifyExist() {
    if (this.verify) {
      this.router.navigate(["/"]);
    } else {
      this.router.navigate(["/creerCompte"]);
    }
  }
}
