import { SousCategorie } from "./../shared/models/sousCategorie.model";
import { Router } from "@angular/router";
import { Categorie } from "./../shared/models/categorie.model";
import { Adresse } from "./../shared/models/adresse.model";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AuthentificationService } from "./../shared/services/authentification.service";
import { Utilisateur } from "./../shared/models/utilisateur.model";
import { Annonce } from "./../shared/models/annonce.model";
import { Component, OnInit } from "@angular/core";
import { zipcodeValidator } from "../shared/validator/validator";
import { OWL_DATE_TIME_LOCALE, OwlDateTimeIntl } from "ng-pick-datetime";
import { DefaultIntl } from "../shared/utile/defaultIntl";
import { CategorieService } from "../shared/services/categorie.service";
import { AnnonceService } from "../shared/services/annonce.service";
import { SessionService } from "../shared/services/session.service";
@Component({
  selector: "app-deposer",
  templateUrl: "./deposer.component.html",
  styleUrls: ["./deposer.component.css"],
  providers: [
    { provide: OwlDateTimeIntl, useClass: DefaultIntl },
    { provide: OWL_DATE_TIME_LOCALE, useValue: "fr" }
  ]
})
export class DeposerComponent implements OnInit {
  public radio: string = "";
  public minDate = new Date();
  public formGroup: FormGroup;
  public annonce: Annonce;
  public utilisateur: Utilisateur;
  public adresse: Adresse;
  public selectedMoments: Date[];
  public categories: Categorie[];
  public anoTest: Annonce;
  public affiche: string = "Catégorie *";
  //public selectedMoments = [null, null];

  constructor(
    private categorieServie: CategorieService,
    private authService: AuthentificationService,
    private formBuilder: FormBuilder,
    private annonceService: AnnonceService,
    private sessionService: SessionService,
    private router: Router
  ) {
    this.annonce = new Annonce(
      null,
      null,
      null,
      null,
      null,
      null,
      new Date(),
      null,
      null,
      true,
      null,
      null,
      null,
      null,
      null,
      null
    );
    this.adresse = new Adresse(null, null, null, null, null, null, null, null);
    this.authService.utilisateur.subscribe((utilisateur: Utilisateur) => {
      this.utilisateur = utilisateur;
    });
  }

  ngOnInit() {
    this.sessionService.isLogged().then((result: boolean) => {
      if (!result) {
        this.router.navigate(["/connexion"]);
      }
    });

    this.categorieServie.listeCategorie.subscribe((categories: Categorie[]) => {
      this.categories = categories;
    });

    this.formGroup = this.formBuilder.group({
      customRadioInline1: ["", Validators.required],
      dateDebut: ["", Validators.required],
      dateFin: ["", Validators.required],
      /*besoin:  [
        "",
        [Validators.required, Validators.min(1), Validators.max(20)]
      ],*/
      besoin: [""],
      titre: [
        "",
        [Validators.required, Validators.minLength(4), Validators.maxLength(50)]
      ],
      contenu: ["", Validators.required],
      codePostal: ["", zipcodeValidator],
      ville: ["", Validators.required]
    });
    this.formControlValueChanged();
  }

  formControlValueChanged() {
    const besoinConst = this.formGroup.get("besoin");
    this.formGroup
      .get("customRadioInline1")
      .valueChanges.subscribe((mode: string) => {
        console.log(mode);
        if (mode === "demandes") {
          besoinConst.setValidators([Validators.required]);
        } else if (mode === "proposes") {
          besoinConst.clearValidators();
        }
        besoinConst.updateValueAndValidity();
      });
  }

  onSubmit() {
    if (this.formGroup.controls["customRadioInline1"].value == "proposes") {
      this.annonce.offre = true;
    } else {
      this.annonce.demande = true;
    }
    this.utilisateur = JSON.parse(sessionStorage.getItem("session"));
    //let sousCat = document.getElementById;
    this.annonce.nombreBesoin = this.formGroup.controls["besoin"].value;
    this.annonce.dateTimeDebut = this.selectedMoments[0];
    this.annonce.dateTimeFin = this.selectedMoments[1];
    this.annonce.utilisateur = this.utilisateur;
    this.annonce.adresse = this.adresse;
    this.annonceService.save(this.annonce).subscribe((ano: Annonce) => {
      this.anoTest = ano;
      this.annonceService.initAnnonces();
      this.router.navigate(["/annonces"]);
    });
  }

  getCategorie(sousCategorie: SousCategorie) {
    this.affiche = sousCategorie.libelle;
    this.annonce.sousCategorie = sousCategorie;
    /*this.categories.forEach(categorie => {
      if (document.getElementById(categorie.libelle)) {
        let sous = document.getElementById(categorie.libelle);
        this.affiche = sous.nodeValue;
        console.log(sous);
        console.log(this.affiche);
      }
    });*/
  }
}
