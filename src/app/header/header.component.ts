import { Router } from "@angular/router";
import { Utilisateur } from "./../shared/models/utilisateur.model";
import { Component, OnInit, OnChanges } from "@angular/core";
import { AuthentificationService } from "../shared/services/authentification.service";
import { SessionService } from "../shared/services/session.service";
import { Roles } from "../shared/models/roles.model";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"]
})
export class HeaderComponent implements OnInit, OnChanges {
  public logo: string = "assets/img/logo.png";
  public iBenevolart: string = "assets/img/iBenevolart.png";
  public rechercher: string = "assets/img/rechercher.png";
  public deposer: string = "assets/img/deposer.png";
  public connexion: string = "assets/img/connexion.png";
  public deconnexion: string = "assets/img/deconnexion.png";
  public connected: string = "assets/img/smile.png";
  public role: Roles;
  public utilisateur: Utilisateur;
  public verify: boolean;
  public pseudo: Utilisateur = JSON.parse(sessionStorage.getItem("session"));

  constructor(
    private authService: AuthentificationService,
    private sessionService: SessionService,
    private router: Router
  ) {}

  ngOnInit() {
    this.authService.utilisateur.subscribe((utilisateur: Utilisateur) => {
      this.utilisateur = utilisateur;
    });

    this.authService.role.subscribe((role: Roles) => {
      this.role = role;
    });

    this.sessionService.session.subscribe((verify: boolean) => {
      this.verify = verify;
    });

    this.sessionService.isLogged().then((result: boolean) => {
      if (!result) {
        this.verify = false;
      }
    });
  }

  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
    throw new Error("Method not implemented.");
  }

  logout(): void {
    sessionStorage.removeItem("session");

    this.sessionService.session.subscribe((verify: boolean) => {
      this.verify = verify;
    });

    this.sessionService.isLogged().then((result: boolean) => {
      if (!result) {
        this.verify = false;
      }
    });
  }
}
