import { Authentification } from "./../shared/models/authentification.model";
import { Utilisateur } from "./../shared/models/utilisateur.model";
import { Annonce } from "./../shared/models/annonce.model";
import { Component, OnInit } from "@angular/core";
import { AnnonceService } from "../shared/services/annonce.service";
import { AuthentificationService } from "../shared/services/authentification.service";

@Component({
  selector: "app-mes-annonces",
  templateUrl: "./mes-annonces.component.html",
  styleUrls: ["./mes-annonces.component.css"]
})
export class MesAnnoncesComponent implements OnInit {
  public annonces: Annonce[];
  public p: number = 1;
  public count: number = 15;
  public utilisateur: Utilisateur;

  constructor(
    private annonceService: AnnonceService,
    private authService: AuthentificationService
  ) {}

  ngOnInit() {
    this.utilisateur = JSON.parse(sessionStorage.getItem("session"));

    this.annonceService
      .getMesAnnonces(this.utilisateur.id)
      .subscribe((annonces: Annonce[]) => {
        this.annonces = annonces;
      });
  }
}
