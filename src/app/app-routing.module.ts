import { AnnonceContainerComponent } from "./annonce-container/annonce-container.component";
import { ListeAnnonceComponent } from "./listeAnnonce/listeAnnonce.component";
import { DeposerComponent } from "./deposer/deposer.component";
import { RechercherComponent } from "./rechercher/rechercher.component";
import { CreerCompteComponent } from "./creerCompte/creerCompte.component";
import { ConnexionComponent } from "./connexion/connexion.component";
import { AboutComponent } from "./about/about.component";
import { HomeComponent } from "./home/home.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AnnonceDetailsComponent } from "./annonce-container/annonce-details/annonce-details.component";
import { MesAnnoncesComponent } from "./mes-annonces/mes-annonces.component";
import { GererCompteComponent } from "./gerer-compte/gerer-compte.component";
import { EnvoieMessageComponent } from "./annonce-container/envoie-message/envoie-message.component";

const routes: Routes = [
  { path: "", component: HomeComponent },
  { path: "about", component: AboutComponent },
  { path: "connexion", component: ConnexionComponent },
  { path: "creerCompte", component: CreerCompteComponent },
  { path: "rechercher", component: RechercherComponent },
  { path: "deposer", component: DeposerComponent },
  {
    path: "annonce",
    component: AnnonceContainerComponent,
    children: [
      { path: ":id", component: AnnonceDetailsComponent },
      { path: ":id/msg", component: EnvoieMessageComponent }
    ]
  },
  { path: "annonces", component: ListeAnnonceComponent },
  { path: "mesannonces", component: MesAnnoncesComponent },
  { path: "compte", component: GererCompteComponent },
  { path: "**", redirectTo: "connexion" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
