import { OwlDateTimeIntl } from "ng-pick-datetime";

export class DefaultIntl extends OwlDateTimeIntl {
  cancelBtnLabel = "Annuler";
  setBtnLabel = "Appliquer";
  rangeFromLabel = "De";
  rangeToLabel = "A";
  hour12AMLabel = "Matin";
  hour12PMLabel = "Après-midi";
}
