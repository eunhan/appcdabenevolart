import { AbstractControl } from "@angular/forms";

export function zipcodeValidator(control: AbstractControl) {
  if (control && (control.value !== null || control.value !== undefined)) {
    const regex = new RegExp("^[0-9]{5}$");

    if (!regex.test(control.value)) {
      return {
        isError: true
      };
    }
  }

  return null;
}

export function telephoneValidator(control: AbstractControl) {
  if (control && (control.value !== null || control.value !== undefined)) {
    const regex = new RegExp("^[0-9]{10}$");

    if (!regex.test(control.value)) {
      return {
        isError: true
      };
    }
  }

  return null;
}

export function emailValidtor(control: AbstractControl) {
  if (control && (control.value !== null || control.value !== undefined)) {
    const regex = new RegExp(
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    );

    if (!regex.test(control.value)) {
      return {
        isError: true
      };
    }
  }

  return null;
}

/*export function besoinValidator(control: AbstractControl) {
  debugger;
  if (control && (control.value !== null || control.value !== undefined)) {
    const regex = new RegExp(
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    );

    if (control.value <= 20 && control.value > 0) {
      return {
        isError: true
      };
    }
  }

  return null;
}*/

export function passValidator(control: AbstractControl) {
  if (control && (control.value !== null || control.value !== undefined)) {
    const cnfpassValue = control.value;

    const passControl = control.root.get("motPasse"); // magic is this
    if (passControl) {
      const passValue = passControl.value;
      if (passValue !== cnfpassValue || passValue === "") {
        return {
          isError: true
        };
      }
    }
  }

  return null;
}
