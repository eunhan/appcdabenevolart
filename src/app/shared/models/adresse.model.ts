import { Annonce } from "./annonce.model";

export class Adresse {
  constructor(
    public id: number,
    public numVoie: number,
    public typeVoie: string,
    public libelleRue: string,
    public codePostal: string,
    public ville: string,
    public pays: string,
    public adresseSupl
  ) //public ltAnnonce: Annonce[]
  {}
}
