import { Annonce } from "./annonce.model";
export class Url {
  constructor(public id: number, public url: string, public annonce: Annonce) {}
}
