import { Annonce } from "./annonce.model";
import { Utilisateur } from "./utilisateur.model";
export class Message {
  constructor(
    public id: number,
    public idCorrespondant: number,
    public envoi: boolean,
    public recu: boolean,
    public objet: string,
    public contenu: string,
    public dateTimeMsg: Date,
    public archive: boolean,
    public abus: boolean,
    public commentaire: boolean,
    public msg: boolean,
    public utilisateur: Utilisateur,
    public annonce: Annonce
  ) {}
}
