import { Annonce } from "./annonce.model";
import { Message } from "./message.model";
import { Roles } from "./roles.model";

export class Utilisateur {
  constructor(
    public id: number,
    public pseudo: string,
    public telephone: string,
    public avertissement: number,
    public active: boolean,
    public roles: Roles,
    public ltMessage: Message[],
    public ltAnnonceMettre: Annonce[]
  ) {}
}
