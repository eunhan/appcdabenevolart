import { Categorie } from "./categorie.model";
import { Annonce } from "./annonce.model";
export class SousCategorie {
  constructor(
    public id: number,
    public libelle: string,
    /*public ltAnnonce: Annonce[],*/
    public categorie: Categorie
  ) {}
}
