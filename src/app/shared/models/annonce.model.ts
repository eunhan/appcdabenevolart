import { Utilisateur } from "./utilisateur.model";
import { Adresse } from "./adresse.model";
import { Url } from "./url.model";
import { Message } from "./message.model";
import { SousCategorie } from "./sousCategorie.model";
export class Annonce {
  constructor(
    public id: number,
    public offre: boolean,
    public demande: boolean,
    public objet: string,
    public contenu: string,
    public nombreBesoin: number,
    public dateTimePub: Date,
    public dateTimeDebut: Date,
    public dateTimeFin: Date,
    public visibilite: boolean,
    public ltMessage: Message[],
    public ltUrl: Url[],
    public sousCategorie: SousCategorie,
    public adresse: Adresse,
    public utilisateur: Utilisateur,
    public ltBenevol: Utilisateur[]
  ) {}
}
