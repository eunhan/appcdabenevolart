import { Utilisateur } from "./utilisateur.model";

export class Authentification {
  constructor(
    public id: number,
    public email: string,
    public motPasse: string,
    public utilisateur: Utilisateur
  ) {}
}
