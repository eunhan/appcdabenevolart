import { HttpClient } from "@angular/common/http";
import { Annonce } from "./../models/annonce.model";
import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class AnnonceService {
  public annonces: BehaviorSubject<Annonce[]> = new BehaviorSubject(null);
  public annonce: BehaviorSubject<Annonce>;
  private urlAnnonce: string = "http://localhost:9091/";

  constructor(private http: HttpClient) {
    this.initAnnonces();
  }

  selectAnnonce(index: number): void {
    this.annonce.next(this.annonces.value[index]);
  }

  initAnnonces(): void {
    this.http
      .get<Annonce[]>(this.urlAnnonce + "listAnnonce")
      .subscribe((annonces: Annonce[]) => {
        this.annonces.next(annonces);
      });
  }

  save(anno: Annonce) {
    return this.http.post(this.urlAnnonce + "deposer", anno);
  }

  getAnnonce(index: number) {
    return this.http.get<Annonce>(this.urlAnnonce + "getAnnonce/" + index);
  }

  getMesAnnonces(id: number) {
    return this.http.get<Annonce[]>(this.urlAnnonce + "mesAnnonces/" + id);
  }
}
