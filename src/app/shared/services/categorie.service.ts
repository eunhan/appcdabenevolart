import { HttpClient } from "@angular/common/http";
import { BehaviorSubject } from "rxjs";
import { Categorie } from "./../models/categorie.model";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class CategorieService {
  public listeCategorie: BehaviorSubject<Categorie[]> = new BehaviorSubject(
    null
  );

  private urlCategorie: string = "http://localhost:9091/";

  constructor(private http: HttpClient) {
    this.initCategorie();
  }

  initCategorie(): void {
    this.http
      .get<Categorie[]>(this.urlCategorie + "listeCategorie")
      .subscribe((listeCategorie: Categorie[]) => {
        this.listeCategorie.next(listeCategorie);
      });
  }
}
