import { BehaviorSubject } from "rxjs";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class SessionService {
  public session: BehaviorSubject<boolean> = new BehaviorSubject(null);

  constructor() {}

  isLogged(): Promise<boolean> {
    if (typeof Storage != "undefined") {
      if (sessionStorage.getItem("session")) {
        this.session.next(true);
        return Promise.resolve(true);
      }
    }
    return Promise.resolve(false);
  }
}
