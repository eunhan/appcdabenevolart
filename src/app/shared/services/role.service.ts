import { HttpClient } from "@angular/common/http";
import { BehaviorSubject } from "rxjs";
import { Injectable } from "@angular/core";
import { Roles } from "../models/roles.model";

@Injectable({
  providedIn: "root"
})
export class RoleService {
  public role: BehaviorSubject<Roles> = new BehaviorSubject(null);
  public url: string = "http://localhost:9091/";

  constructor(private http: HttpClient) {}

  /*findRole(id: number) {
    this.http.get(this.url + "findRole" + id).subscribe((role: Roles) => {
      this.role.next(role);
    });
  }*/
}
