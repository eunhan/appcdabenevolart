import { Utilisateur } from "./../models/utilisateur.model";
import { BehaviorSubject, Observable } from "rxjs";
import { Authentification } from "./../models/authentification.model";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Roles } from "../models/roles.model";

@Injectable({
  providedIn: "root"
})
export class AuthentificationService {
  public authentification: BehaviorSubject<
    Authentification
  > = new BehaviorSubject(null);
  public utilisateur: BehaviorSubject<Utilisateur> = new BehaviorSubject(null);
  public role: BehaviorSubject<Roles> = new BehaviorSubject(null);
  public verify: boolean = false;
  private urlBenevol: string = "http://localhost:9091/";

  constructor(private http: HttpClient) {}

  login(authentification: Authentification) {
    let retour = this.http.post(this.urlBenevol + "login", authentification);
    retour.subscribe((user: Utilisateur) => {
      this.utilisateur.next(user);
      this.role.next(user.roles);
    });

    return retour;
  }

  inscription(authentification: Authentification) {
    return this.http.post(this.urlBenevol + "inscription", authentification);
  }

  getAuthentification(id: number) {
    return this.http.post(this.urlBenevol + "getAuthentification", id);
  }

  modifier(authentification: Authentification) {
    console.log(authentification);
    this.http
      .post(this.urlBenevol + "modifier", authentification)
      .subscribe((utilisateur: Utilisateur) => {
        this.utilisateur.next(utilisateur);
        console.log(utilisateur);
        console.log(this.utilisateur);
      });
    //return this.http.post(this.urlBenevol + "modifier", authentification);
  }
}
