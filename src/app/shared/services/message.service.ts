import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Message } from "../models/message.model";

@Injectable({
  providedIn: "root"
})
export class MessageService {
  private urlMsg: string = "http://localhost:9091/";

  constructor(private http: HttpClient) {}

  save(message: Message) {
    
    return this.http.post(this.urlMsg + "saveMsg", message);
  }
}
