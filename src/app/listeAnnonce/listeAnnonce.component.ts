import { Annonce } from "./../shared/models/annonce.model";
import { AnnonceService } from "./../shared/services/annonce.service";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-listeAnnonce",
  templateUrl: "./listeAnnonce.component.html",
  styleUrls: ["./listeAnnonce.component.css"]
})
export class ListeAnnonceComponent implements OnInit {
  public annonces: Annonce[];
  public p: number = 1;
  public count: number = 15;

  constructor(private annonceService: AnnonceService) {}

  ngOnInit() {
    this.annonceService.annonces.subscribe((annonces: Annonce[]) => {
      this.annonces = annonces;
    });
  }

  /* pickAnnonce(index: number) {
    this.annonceService.selectAnnonce(index);
  }*/
}
