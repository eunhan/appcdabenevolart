import { Utilisateur } from "./../shared/models/utilisateur.model";
import { AuthentificationService } from "./../shared/services/authentification.service";
import { Authentification } from "./../shared/models/authentification.model";
import { Component, OnInit } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  Validators,
  EmailValidator
} from "@angular/forms";
import { Router } from "@angular/router";
import { emailValidtor } from "../shared/validator/validator";

@Component({
  selector: "app-connexion",
  templateUrl: "./connexion.component.html",
  styleUrls: ["./connexion.component.css"]
})
export class ConnexionComponent implements OnInit {
  public authForm: FormGroup;
  public utilisateur: Utilisateur;
  public authentification: Authentification;
  public verify: boolean = true;
  public active: boolean = true;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthentificationService,
    private rooter: Router
  ) {
    this.authentification = new Authentification(null, "", "", null);
  }

  ngOnInit() {
    this.authForm = this.formBuilder.group({
      email: ["", emailValidtor],
      motPasse: ["", Validators.required]
    });
  }

  onSubmit() {
    this.verify = true;
    this.active = true;

    this.authService
      .login(this.authentification)
      .subscribe((utilisateur: Utilisateur) => {
        this.utilisateur = utilisateur;
        if (this.utilisateur !== null) {
          if (!this.utilisateur.active) {
            this.active = false;
            this.rooter.navigate(["/connexion"]);
          } else if (typeof Storage !== "undefined") {
            sessionStorage.setItem("session", JSON.stringify(this.utilisateur));
            this.rooter.navigate(["/rechercher"]);
          }
        } else {
          this.verify = false;
          this.rooter.navigate(["/connexion"]);
        }
      });
  }
}
