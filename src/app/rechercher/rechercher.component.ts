import { Categorie } from "./../shared/models/categorie.model";
import { SessionService } from "./../shared/services/session.service";
import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { CategorieService } from "../shared/services/categorie.service";

@Component({
  selector: "app-rechercher",
  templateUrl: "./rechercher.component.html",
  styleUrls: ["./rechercher.component.css"]
})
export class RechercherComponent implements OnInit {
  public categories: Categorie[];
  public selectedMoments: Date[];
  public minDate = new Date();
  public dates: Date[];
  public affiche: string = "Catégorie *";
  public myMap: Map<string, string> = new Map([
    ["type", null],
    ["categorie", null],
    ["cle", null],
    ["ville", null],
    ["debut", null],
    ["fin", null]
  ]);

  constructor(
    private sessionService: SessionService,
    private router: Router,
    private categorieServie: CategorieService
  ) {}

  ngOnInit() {
    this.sessionService.isLogged().then((result: boolean) => {
      if (!result) {
        this.router.navigate(["/connexion"]);
      }
    });

    this.categorieServie.listeCategorie.subscribe((categories: Categorie[]) => {
      this.categories = categories;
    });
  }

  onSubmit() {
    this.myMap.set("debut", this.dates[0].toString());
    this.myMap.set("fin", this.dates[1].toString());
  }

  getType(type: string) {
    switch (type) {
      case "proposes": {
        this.myMap.set("type", "offre");
        break;
      }
      case "demandes": {
        this.myMap.set("type", "demande");
      }
    }
  }

  getCategorie(sousCategorie: string) {
    this.affiche = sousCategorie;
    this.myMap.set("categorie", sousCategorie);
  }
}
