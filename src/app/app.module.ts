import { MessageService } from "./shared/services/message.service";
import { AnnonceDetailsComponent } from "./annonce-container/annonce-details/annonce-details.component";
import { AnnonceService } from "./shared/services/annonce.service";
import { AuthentificationService } from "./shared/services/authentification.service";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { HeaderComponent } from "./header/header.component";
import { HomeComponent } from "./home/home.component";
import { AboutComponent } from "./about/about.component";
import { FooterComponent } from "./footer/footer.component";
import { ConnexionComponent } from "./connexion/connexion.component";
import { CreerCompteComponent } from "./creerCompte/creerCompte.component";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { RechercherComponent } from "./rechercher/rechercher.component";
import { DeposerComponent } from "./deposer/deposer.component";
import { ListeAnnonceComponent } from "./listeAnnonce/listeAnnonce.component";
import { OwlDateTimeModule, OwlNativeDateTimeModule } from "ng-pick-datetime";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { CategorieService } from "./shared/services/categorie.service";
import { SessionService } from "./shared/services/session.service";
import { NgxPaginationModule } from "ngx-pagination";
import { MesAnnoncesComponent } from "./mes-annonces/mes-annonces.component";
import { GererCompteComponent } from "./gerer-compte/gerer-compte.component";
import { EnvoieMessageComponent } from "./annonce-container/envoie-message/envoie-message.component";
import { AnnonceContainerComponent } from "./annonce-container/annonce-container.component";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    AboutComponent,
    FooterComponent,
    ConnexionComponent,
    CreerCompteComponent,
    RechercherComponent,
    DeposerComponent,
    ListeAnnonceComponent,
    MesAnnoncesComponent,
    GererCompteComponent,
    EnvoieMessageComponent,
    AnnonceDetailsComponent,
    AnnonceContainerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    BrowserAnimationsModule,
    NgxPaginationModule
  ],
  providers: [
    AuthentificationService,
    AnnonceService,
    CategorieService,
    SessionService,
    MessageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
