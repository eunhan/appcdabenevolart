import { Utilisateur } from "../../shared/models/utilisateur.model";
import { Annonce } from "../../shared/models/annonce.model";
import { AnnonceService } from "../../shared/services/annonce.service";
import { Component, OnInit, OnDestroy } from "@angular/core";
import { ActivatedRoute, Params } from "@angular/router";

@Component({
  selector: "app-annonce-details",
  templateUrl: "./annonce-details.component.html",
  styleUrls: ["./annonce-details.component.css"]
})
export class AnnonceDetailsComponent implements OnInit, OnDestroy {
  public annonce: Annonce;
  public index: number;
  public detail: string = "";
  public id: number;
  public sub: any;
  public idUtilisateur: Utilisateur = JSON.parse(
    sessionStorage.getItem("session")
  );

  constructor(
    private annonceService: AnnonceService,
    private activateRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.sub = this.activateRoute.params.subscribe(params => {
      this.id = +params["id"];
      this.annonceService.getAnnonce(this.id).subscribe((annonce: Annonce) => {
        this.annonce = annonce;
      });
    });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }
  getUrl(): string[] {
    return ["/annonce", this.id + "", "msg"];
  }
}
