import { MessageService } from "./../../shared/services/message.service";
import { Utilisateur } from "./../../shared/models/utilisateur.model";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { AnnonceService } from "./../../shared/services/annonce.service";
import { Component, OnInit } from "@angular/core";
import { Annonce } from "src/app/shared/models/annonce.model";
import { Message } from "src/app/shared/models/message.model";

@Component({
  selector: "app-envoie-message",
  templateUrl: "./envoie-message.component.html",
  styleUrls: ["./envoie-message.component.css"]
})
export class EnvoieMessageComponent implements OnInit {
  public annonce: Annonce;
  public sub: any;
  public id: number;
  public msgForm: FormGroup;
  public msg: Message;
  public msgCorrespondant: Message;
  public utilisateur: Utilisateur = JSON.parse(
    sessionStorage.getItem("session")
  );

  constructor(
    private annonceService: AnnonceService,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private messageService: MessageService
  ) {
    this.msg = new Message(
      null,
      null,
      true,
      false,
      null,
      null,
      new Date(),
      false,
      false,
      false,
      true,
      null,
      null
    );
    this.msgCorrespondant = new Message(
      null,
      null,
      false,
      true,
      null,
      null,
      new Date(),
      false,
      false,
      false,
      true,
      null,
      null
    );
  }

  ngOnInit() {
    this.sub = this.activatedRoute.params.subscribe(params => {
      this.id = +params["id"];
      this.annonceService.getAnnonce(this.id).subscribe((annonce: Annonce) => {
        this.annonce = annonce;
      });
    });

    this.msgForm = this.formBuilder.group({
      msg: ["", Validators.required]
    });
  }

  onSubmit() {
    this.msg.idCorrespondant = this.annonce.utilisateur.id;
    this.msg.utilisateur = this.utilisateur;
    this.msg.annonce = this.annonce;
    this.messageService.save(this.msg);

    this.msgCorrespondant.utilisateur = this.annonce.utilisateur;
    this.msgCorrespondant.idCorrespondant = this.utilisateur.id;
    this.msgCorrespondant.annonce = this.annonce;
    this.messageService.save(this.msgCorrespondant);
  }
}
